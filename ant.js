class Ant{
    constructor(setUp) {
        this.position=(setUp&&setUp.position)?setUp.position:{x:0,y:0};
        this.name="worker";
        this.food=0;
        this.direction=random(["x","xy","y","-xy","-x","-x-y","-y","x-y"]);
        this.inventary={
            food:0
            ,trail:0
        }
        this.job=(setUp&&setUp.job)?setUp.job:["nostalgia","getFood","trail","returnFood","follow","move"];
        this.step=0;
        this.mem=[];
        this.nexObserb=0;
        this.eventTick = global.onTick(()=>{this.tick();});
        this.nextFollowSearch=0;
        this.followTarget=false;
        this.qween=(setUp&&setUp.qween)?setUp.qween:false;
        this.trailingStep=0;
        this.nostalgia=(setUp&&setUp.nostalgia)?setUp.nostalgia:0;
        this.parameter=(setUp&&setUp.parameter)?setUp.parameter:{
            "nostalgia":[200,200]
        };
    }
    tick(){
        if(
            Math.abs(this.position.x-this.qween.position.x)<global.parameter.interactDistance
            &&Math.abs(this.position.y-this.qween.position.y)<global.parameter.interactDistance
            &&this.name!="qween"
        )this.closeQween();

        let done=false;
        let tryng=0;
        do {
            switch (this.job[tryng]) {
                case "nostalgia":{ done=this.nostalgic(); break;}
                case "getFood":{ done=this.getFood(); break;}
                case "trail":{ done=this.trail(); break;}
                case "returnFood":{ done=this.returnFood(); break;}
                case "follow":{ done=this.trailFollow(); break;}
                case "move":{ done=this.move(); break;}
                default:{ done=this.move(); break;}
            }
            tryng++;
        } while (tryng<this.job.length&&done==false);

        if(!done)this.kill();
    }
    closeQween(){
        this.mem=[JSON.parse(JSON.stringify(this.qween.position))];
        this.qween.food+=this.food;
        this.food=0;
        this.goingBack=false;
        this.nostalgia=0;
    }
    getFood(){
        let r = true;
        if(this.food<global.parameter.foodCapacity){
            if(this.nexObserb<1){
                let round = this.obserb("food");
                let p = this.position;
                if(round.length>1){
                    let clouser = round[0];
                    let distance=Math.abs(p.x-clouser.position.x)+Math.abs(p.y-clouser.position.y);
                    if(round.length>0 && distance<global.parameter.interactDistance){
                        clouser.units--; this.food++;
                        if(clouser.units==0)global.item.splice(global.item.indexOf(clouser),1);
                        return true;
                    }else{
                        this.nexObserb=Math.floor((distance-global.parameter.interactDistance)/global.parameter.move);
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                this.nexObserb--;
                return false;
            }
        }else{
            this.nexObserb=0;
            return false;
        }
    }
    move(rand){
        rand=typeof rand=="undefined"?true:rand;
        if(random(global.parameter.direccionRandom)==1&&rand){
            this.direction = random(["y","xy","x","x-y","-y","-x-y","-x","-xy"]);
            this.mem.push(JSON.parse(JSON.stringify(this.position)));
        }else{
            switch(this.direction){
                case "y":
                this.position.y+=global.parameter.move;
                break;
                case "xy":
                this.position.x+=global.parameter.move/2;
                this.position.y+=global.parameter.move/2;
                break;
                case "x":
                this.position.x+=global.parameter.move;
                break;
                case "x-y":
                this.position.x+=global.parameter.move/2;
                this.position.y-=global.parameter.move/2;
                break;
                case "-y":
                this.position.y-=global.parameter.move;
                break;
                case "-x-y":
                this.position.x-=global.parameter.move/2;
                this.position.y-=global.parameter.move/2;
                break;
                case "-x":
                this.position.x-=global.parameter.move;
                break;
                case "-xy":
                this.position.x-=global.parameter.move/2;
                this.position.y+=global.parameter.move/2;
                break;
            }
            if(
                (
                    Math.abs(this.position.x)>global.parameter.tableW/2
                    || Math.abs(this.position.y)>global.parameter.tableH/2
                )
                && global.ants.indexOf(this)>=0
            ){
                this.kill();
            }
        }
        return true;
    }
    trail(){
        this.trailingStep=(this.trailingStep+1)%global.parameter.trailingSpace;
        if(this.trailingStep==0 && this.food == global.parameter.foodCapacity){
            let a=new Item("trail",JSON.parse(JSON.stringify(this.position)));
            a.units=global.parameter.trailingQuant;
            global.item.push(a);
            return true;
        }else {
            return false;
        }
    }
    trailFollow(){
        if(this.nextFollowSearch<1){
            let direction = {
                "x":0
                ,"xy":0
                ,"y":0
                ,"-xy":0
                ,"-x":0
                ,"-x-y":0
                ,"-y":0
                ,"x-y":0
            };
            let dirBigger=["x","xy","y","-xy","-x","-x-y","-y","x-y"];
            let p = this.position;
            let obserDirection=this.obserDirection();
            let target = global.item.filter(a=>{
                return (
                    a.name=="trail"
                );
            }).filter(a=>{
                let dis = Math.abs(this.position.x-a.position.x)+Math.abs(this.position.y-a.position.y);
                return (
                    dis>global.parameter.followRange[0]
                    &&dis<global.parameter.followRange[1]
                )
            }).filter(a=>{
                return obserDirection.includes(this.directionTo(a.position).d);
            }).forEach((a)=>{
                direction[this.directionTo(a.position).d]++;
                a.units--;
                if(a.units<1)global.item.splice(global.item.indexOf(a),1);
            });
            let bigger = dirBigger.sort((a,b)=>{ return direction[b]-direction[a]; })[0];
            if(direction[bigger]>0){
                if(this.direction!=bigger){
                    this.direction=bigger;
                    this.mem.push(JSON.parse(JSON.stringify(this.position)));
                }
                this.move(false);
                this.nextFollowSearch=global.parameter.followFreq;
                this.followTarget=true;
                return true;
            }else{
                this.nextFollowSearch=global.parameter.followFreq;
                this.followTarget=false;
                return false;
            }
        }else if(this.followTarget){
            this.nextFollowSearch--;
            this.move(false);
            return true;
        }else{
            this.nextFollowSearch--;
            return false;
        }
    }
    returnFood(){
        if( this.food<global.parameter.foodCapacity ){
            return false;
        }else{
            return this.goBack();
        }
    }
    nostalgic(){
        if(this.nostalgia>=random(this.parameter.nostalgia[1],this.parameter.nostalgia[0])-1){
            this.nostalgia=this.parameter.nostalgia[1];
            if(this.goBack()){
                return true;
            }else{
                this.nostalgia=0;
                return false;
            }
        }else{
            this.nostalgia++;
            return false;
        }
    }
    goBack(){
        if(
            (
                Math.abs(this.position.x)==global.parameter.interactDistance
                &&Math.abs(this.position.y)==global.parameter.interactDistance
                &&this.name!="qween"
            )
            || this.mem.length<1
        ){
            return false;
        }else{
            let goTo = this.mem[this.mem.length-1];
            this.moveTo(goTo);
            if(
                Math.abs(this.position.x-goTo.x)<global.parameter.interactDistance
                &&Math.abs(this.position.y-goTo.y)<global.parameter.interactDistance
            ) this.mem.pop();
            return true;
        }
    }
    moveTo(toPos){
        if(
            Math.abs(toPos.x-this.position.x)>(global.parameter.move/2)
            &&Math.abs(toPos.y-this.position.y)>(global.parameter.move/2)
        ){
            this.position.x+=(toPos.x>this.position.x)?
                (Math.abs(this.position.x-toPos.x)<(global.parameter.move/2)?Math.abs(this.position.x-toPos.x):global.parameter.move/2)
                :(Math.abs(this.position.x-toPos.x)<(global.parameter.move/2)?Math.abs(this.position.x-toPos.x)*-1:(global.parameter.move/2)*-1)
            this.position.y+=(toPos.y>this.position.y)?
                (Math.abs(this.position.y-toPos.y)<(global.parameter.move/2)?Math.abs(this.position.y-toPos.y):global.parameter.move/2)
                :(Math.abs(this.position.y-toPos.y)<(global.parameter.move/2)?Math.abs(this.position.y-toPos.y)*-1:(global.parameter.move/2)*-1)
        }else if(Math.abs(toPos.x-this.position.x)>(global.parameter.move/2)){
            this.position.x+=(toPos.x>this.position.x)?
                (Math.abs(this.position.x-toPos.x)<global.parameter.move?Math.abs(this.position.x-toPos.x):global.parameter.move)
                :(Math.abs(this.position.x-toPos.x)<global.parameter.move?Math.abs(this.position.x-toPos.x)*-1:global.parameter.move*-1)
        }else{
            this.position.y+=(toPos.y>this.position.y)?
                (Math.abs(this.position.y-toPos.y)<global.parameter.move?Math.abs(this.position.y-toPos.y):global.parameter.move)
                :(Math.abs(this.position.y-toPos.y)<global.parameter.move?Math.abs(this.position.y-toPos.y)*-1:global.parameter.move*-1)
        }
    }
    obserb(name){
        var p = this.position;
        return global.item.filter(a=>{return a.name==name;})
        .sort((a,b)=>{
            return (Math.abs(p.x-a.position.x)+Math.abs(p.y-a.position.y))-(Math.abs(p.x-b.position.x)+Math.abs(p.y-b.position.y))
        });
    }
    kill(){
        global.ants.splice(global.ants.indexOf(this),1);
        // global.removeTick(this.eventTick);
        // SOLUCION TEMPORAL
        this.tick=(()=>{});
        this.name="rip";
    }
    directionTo(d){
        let grados = (Math.atan2(d.y-this.position.y,d.x-this.position.x)* 180 / Math.PI).toFixed(2);
        let dir;
        return{d:({
                8:"-x"
                ,7:"-xy"
                ,6:"y"
                ,5:"xy"
                ,4:"x"
                ,3:"x-y"
                ,2:"-y"
                ,1:"-x-y"
                ,0:"-x"
            })[(Math.round((grados)*8/360))+4]
            ,g:grados
        };
    }
    obserDirection(){
        return ({
            //112º
            "x":["x","xy","x-y"]
            ,"y":["y","xy","-xy"]
            ,"-x":["-x","-xy","-x-y"]
            ,"-y":["-y","x-y","-x-y"]
            ,"xy":["xy","x","y"]
            ,"x-y":["x-y","x","-y"]
            ,"-xy":["-xy","-x","y"]
            ,"-x-y":["-x-y","-x","-y"]
            //202º
            // "x":["y","xy","x","x-y","-y"]
            // ,"xy":["-xy","y","xy","x","x-y"]
            // ,"y":["-x","-xy","y","xy","x"]
            // ,"-xy":["-x-y","-x","-xy","y","xy"]
            // ,"-x":["-y","-x-y","-x","-xy","y"]
            // ,"-x-y":["x-y","-y","-x-y","-x","-xy"]
            // ,"-y":["x","x-y","-y","-x-y","-x"]
            // ,"x-y":["xy","x","x-y","-y","-x-y"]
        })[this.direction];
    }
}
class Qween extends Ant{
    constructor(){
        super({});
        this.name="qween";
        this.nest = false;
        this.food=global.parameter.foodStart;
        this.createJob=["nostalgia","getFood","trail","returnFood","follow","move"]
    }
    makeAnt(){
        if(this.food>0&&global.ants.length<global.parameter.maxAnts){
            global.ants.push(
                new Ant({
                    job:this.createJob
                    ,position:JSON.parse(JSON.stringify(this.position))
                    ,qween:this
                })
            );
            this.food--;
        }
    }
    tick(){
        if(this.nest){
            if(random(global.parameter.antRandom)==1){
                this.makeAnt();
            }
        }else{
            if(random(global.parameter.nestRandom)==1){
                this.nest=true;
            }else{
                this.move();
            }
        }
    }
}
