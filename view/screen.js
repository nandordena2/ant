class Screen {
    constructor(element) {
        this.window = document.createElement('div');
        this.window.className='window';
        this.window.addEventListener("mousemove",event=>{this.windowsMove(event);});
        element.appendChild(this.window);
        this.table = document.createElement('div');
        this.table.className='table';
        this.table.style.width=global.parameter.tableW+"px";
        this.table.style.height=global.parameter.tableH+"px";
        this.window.appendChild(this.table);
        // let c  = document.createElement('div');
        // c.className='center';
        // this.table.appendChild(c);
        global.onTick(()=>{this.tick();});
    }
    windowsMove(event){
        this.dimencions={};
        this.dimencions.cx = event.clientX;
        this.dimencions.cy = event.clientY;
        this.dimencions.ww = window.innerWidth;
        this.dimencions.wh = window.innerHeight
        this.dimencions.tw = global.parameter.tableW;
        this.dimencions.th = global.parameter.tableH;
        this.dimencions.wd = this.dimencions.tw-this.dimencions.ww;
        this.dimencions.hd = this.dimencions.th-this.dimencions.wh;
        this.dimencions.pcx = this.dimencions.cx/this.dimencions.ww;
        this.dimencions.pcy = this.dimencions.cy/this.dimencions.wh;
        this.dimencions.x = (this.dimencions.wd*this.dimencions.pcx)*-1;
        this.dimencions.y = (this.dimencions.hd*this.dimencions.pcy)*-1;
        this.table.style.left = this.dimencions.x+"px";
        this.table.style.top = this.dimencions.y+"px";
    }
    graf(){
        if(!this.egraf){
            this.egraf = document.createElement('div');
            this.egraf.className='graf';
            this.window.appendChild(this.egraf);
        }
        let text="////// GRAF //////";
        text+="<br>TPS:"+(
            Math.round(
                (1/(((new Date()).getTime()-global.time)/1000))
            )
        );
        text+="<br>ANTs:"+global.ants.length;
        text+="<br>Tick:"+(Math.round(((new Date()).getTime()-global.time)/100));
        text+="<br>Seeds:"+global.item.filter(a=>{return a.name=="food"}).length;
        text+="<br>Food:"+global.ants[0].food;
        text+="<br>Trails:"+global.item.filter(a=>{return a.name=="trail"}).length
        this.egraf.innerHTML=text;
        global.time = (new Date()).getTime();
    }
    drawAnts(){
        if(this.table.querySelector(".qween")==null){
            this.qween = global.ants[0];
            this.eQween = document.createElement('div');
            this.eQween.className='ant qween';
            this.table.appendChild(this.eQween);
        }
        if(this.table.querySelector(".ants")==null){
            this.eAnts = document.createElement('div');
            this.eAnts.className='ant ants';
            this.table.appendChild(this.eAnts);
        }
        if(!this.qween.nest){
            this.eQween.style.left=((global.parameter.tableW/2)+this.qween.position.x)+"px";
            this.eQween.style.top=((global.parameter.tableH/2)+(this.qween.position.y)*-1)+"px";
        }
        let shadow = (global.ants.filter(a=>{return a.name!="qween"&&a.name!="rip"}).map(a=>{
            let p = (a.position.x)+"px "+(a.position.y)*-1+"px 0 0 #000";
            if(global.parameter.drawDistances){
                p+=","+(a.position.x)+"px "+(a.position.y)*-1+"px 0 "+global.parameter.interactDistance+"px #0002";
                p+=","+(a.position.x)+"px "+(a.position.y)*-1+"px 0 "+global.parameter.followRange[0]+"px #0002";
                p+=","+(a.position.x)+"px "+(a.position.y)*-1+"px 0 "+global.parameter.followRange[1]+"px #0002";
            }
            return p;
        })).toString();
        this.eAnts.style.boxShadow = shadow;
    }
    drawFood(){
        if(this.table.querySelector(".seed")==null){
            this.eSeed = document.createElement('div');
            this.eSeed.className='seed';
            this.table.appendChild(this.eSeed);
        }
        let shadow = (global.item.filter(a=>{return a.name=="food"}).map(a=>{
            return a.position.x+"px "+a.position.y*-1+"px 0 0 #c5a64f";
        })).toString();
        this.eSeed.style.boxShadow = shadow;
    }
    drawTrail(){
        if(this.table.querySelector(".trail")==null){
            this.eTrail = document.createElement('div');
            this.eTrail.className='trail';
            this.eTrail.style.left=(global.parameter.tableW/2)+"px";
            this.eTrail.style.top=(global.parameter.tableH/2)+"px";
            this.table.appendChild(this.eTrail);
        }
        let shadow = (global.item.filter(a=>{return a.name=="trail"}).map(a=>{
            return a.position.x+"px "+a.position.y*-1+"px 0 0 #fbe8b2";
        })).toString();
        this.eTrail.style.boxShadow = shadow;
    }
    tick(){
        this.drawAnts();
        this.drawFood();
        this.drawTrail();
        this.graf();
    }
}
