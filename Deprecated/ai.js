class ai{
    constructor(input,output,target){
        this.table = table;
        this.output = output;
        this.input = input;
        this.target = target;
        this.memory = {
            A:new mem()
            ,B:new mem()
            ,C:new mem()
            ,D:new mem()
            ,E:new mem()
            ,now:new mem()
        };
    }
    decide(){
        let same2 = Object.values(this.memory)
        .filter(a=>{ return a.target!=null; })
        .filter(a=>{
            for(i in a.path){
                if(
                    a.path[i].i == this.memory.now.last()
                    && (a.path.prevew(i)).i == this.memory.now.beforelast()
                    && (a.path[i+1])
                ){
                    return a
                }
            }
        });
        if(same2.length > 0 ){
            return ant.random(same2);
        }
        return
    }
    randomDecide(){
        let o = this.output[(
            Math.round(
                Math.random()*this.output.length
            )%this.output.length
        )];
        this.memory.now.path.push({"i":input,"o":o});
        return o;
    }
}
class mem{
    constructor(){
        this.target:false;
        this.tick:Infinity;
        this.path:[];
    }
    prevew(i){
        return (this.path[i-1])||null;
    }
    last(){
        return this.path[this.path.length-1]||null;
    }
    beforelast(){
        return this.path[this.path.length-2]||null;
    }
}
