function compare(direction,a,b){
    return (
        (
            direction=="x"
            && a.x<b.x
            && (b.x-a.x)>=Math.abs(b.y-a.y)
        )
        ||(
            direction=="y"
            && a.y<b.y
            && (b.y-a.y)>=Math.abs(b.x-a.x)
        )
        ||(
            direction=="-x"
            && a.x>b.x
            && (a.x-b.x)>=Math.abs(b.y-a.y)
        )
        ||(
            direction=="-y"
            && a.y>b.y
            && (a.y-b.y)>=Math.abs(b.x-a.x)
        )
        ||(
            direction=="xy"
            && a.x<b.x
            && a.y<b.y
        )
        ||(
            direction=="x-y"
            && a.x<b.x
            && a.y>b.y
        )
        ||(
            direction=="-xy"
            && a.x>b.x
            && a.y<b.y
        )
        ||(
            direction=="-x-y"
            && a.x>b.x
            && a.y>b.y
        )
    )
}
a={x:0,y:0};
directions=["x", "x-y", "-y", "-x-y", "-x", "-xy", "y", "xy"];
positions=[
    {x:1,y:2,name:1}
    ,{x:2,y:1,name:2}
    ,{x:2,y:-1,name:3}
    ,{x:1,y:-2,name:4}
    ,{x:-1,y:-2,name:5}
    ,{x:-2,y:-1,name:6}
    ,{x:-2,y:1,name:7}
    ,{x:-1,y:2,name:8}
    ];
result=[];
directions.forEach(d=>{
    result[d]=[];
    positions.forEach(p=>{
        result[d][p.name]=compare(d,a,p);
    });
});
