function random(to,from){
    if(from&&typeof to == "number")to-=from;
    let count = to, array = false, rand;
    if(typeof to == "object"){
        count = to.length;
        array = true;
    }
    if(count>1) rand = Math.round(Math.random()*count)%count;
    else rand = count;
    if(from&&typeof to == "number")rand+=from;
    return array?to[rand]:rand+1;
}
class Settings{
    constructor() {
        this.parameter={
            foodCapacity:5
            ,interactDistance:20
            ,move:2//need par
            ,maxAnts:200
            ,maxFood:20
            ,foodRandom:2
            ,antRandom:10
            ,nestRandom:300
            ,foodStart:20
            ,direccionRandom:50
            ,tick:30
            ,foodUnits:[500,1000]
            ,tableW:889
            ,tableH:889
            ,trailingQuant:10
            ,trailingSpace:5
            ,followRange:[5,15]
            ,followFreq:10
            ,drawDistances:false
        }
        this.Ctiks=0;
        this.time=(new Date()).getTime();
        this.ants=[];
        this.eventsTicks=[];
        this.item=[];
        this.itemMap=[];
        this.pause=false;
    }
    tick(){
        if(!this.pause){
            this.eventsTicks.forEach(a=>{a();});
            setTimeout(()=>{this.tick()},this.parameter.tick);
            this.Ctiks++;
        }
    }
    onTick(f){
        let a = this.eventsTicks.push(f);
        return this.eventsTicks[a];
    }
    removeTick(ev){
        this.eventsTicks.splice(this.eventsTicks.indexOf(ev),1);
    }
}
