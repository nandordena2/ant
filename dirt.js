class Dirt{
    constructor() {
        global.onTick(()=>{this.tick();});
    }
    makeFood(){
        if(global.item.filter(a=>{return a.name=="food"}).length<global.parameter.maxFood){
            let food = new Item(
                "food"
                ,{
                    x:random(global.parameter.tableW/2,(global.parameter.tableW/2)*-1)
                    ,y:random(global.parameter.tableH/2,(global.parameter.tableH/2)*-1)
                }
            )
            food.units=Math.floor(Math.random()*(
                global.parameter.foodUnits[1]-global.parameter.foodUnits[0]
            ))+global.parameter.foodUnits[0];
            global.item.push(food);
        }
    }
    tick(){
        if(random(global.parameter.foodRandom)==1)this.makeFood();
    }
}
class Item{
    constructor(name,position){
        this.name=name||"air";
        this.position=position||{x:0,y:0};
        if(!global.itemMap[this.position.x])global.itemMap[this.position.x]=[];
        if(!global.itemMap[this.position.x][this.position.y])global.itemMap[this.position.x][this.position.y]=[];
        global.itemMap[this.position.x][this.position.y].push(this);
    }
}
